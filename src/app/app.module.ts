import { NgModule } from '@angular/core';
import { FormsModule }  from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

import  { UsersComponent }  from './components/users/users.component';
import { TableComponent } from './components/table/table.component';
import { ModalComponent } from './components/modal/modal.component'
@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    TableComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
