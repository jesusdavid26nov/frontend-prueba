export class User {
  constructor(
    _id?: string,
    name?: string,
    lastname?: string,
    idCard?: string,
    email?: string,
    phone?: string
  ) {
    this._id = _id || '';
    this.name = name || '';
    this.lastname = lastname || '';
    this.idCard = idCard || '';
    this.email = email || '';
    this.phone = phone || '';
  }
  _id: string;
  name: string;
  lastname: string;
  idCard: string;
  email: string;
  phone: string;
}
