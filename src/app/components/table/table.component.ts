import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Column } from '../interface';
import { UserService } from '../../services/user.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  providers: [UserService],
})
export class TableComponent {
  constructor(private userService: UserService) {}
  @Input()
  column: Column[] = [];
  @Input()
  data: any[] = [];
  @Output() onGetUser = new EventEmitter<string>();
  @Output() onOpenModal = new EventEmitter<any>();

  crud: string = 'crud';

  editUser(user: User) {
    this.onOpenModal.emit({ modal: 'open modal', user: user });
  }

  deleteUser(_id: string) {
    if (confirm('Are you sure you want to delete it?')) {
      this.userService.deleteUser(_id).subscribe((res) => {
        this.getUsers();
      });
    }
  }

  getUsers() {
    this.onGetUser.emit('get user');
  }
}
