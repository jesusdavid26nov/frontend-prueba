import { Component, OnInit } from '@angular/core';
import { Column } from '../interface';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [UserService],
})
export class UsersComponent implements OnInit {
  constructor(public userService: UserService) {}
  modalOpen: boolean = false;
  userEdit: any;
  ngOnInit(): void {
    this.getUsers();
  }
  column: Column[] = [
    {
      label: 'Nombre',
      key: 'name',
    },
    {
      label: 'Apellido',
      key: 'lastname',
    },
    {
      label: 'Email',
      key: 'email',
    },
    {
      label: 'Telefono',
      key: 'phone',
    },
    {
      label: 'Acciones',
      key: 'crud',
    },
  ];

  openModal(): void {
    this.modalOpen = !this.modalOpen;
  }
  onOpenModalEdit({ modal, user }: { modal: string; user: User | any }): void {
    if (modal === 'open modal') {
      this.modalOpen = !this.modalOpen;
      this.userService.getUser(user).subscribe((res: any) => {
        this.userEdit = res;
      });
    }
  }
  processMessage(message: any) {
    this.modalOpen = message;
  }

  getUsers() {
    this.userService.getUsers().subscribe((res: any) => {
      this.userService.users = res;
    });
  }

  onGetUsers(users: any) {
    if (users === 'get user') {
      this.getUsers();
    }
  }
}
