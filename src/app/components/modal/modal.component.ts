import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import validator from 'validator';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  providers: [UserService],
})
export class ModalComponent {
  constructor(public userService: UserService) {}
  @Input() modal: boolean = false;
  @Input() userEdit: User | any;
  @Output() closeModal = new EventEmitter<boolean>();
  @Output() onGetUser = new EventEmitter<any>();

  onCloseModal(form?: NgForm ) {
    this.closeModal.emit(!this.modal);
    this.modal = !this.modal;
    this.resetForm(form);
  }

  addUser(form?: NgForm | any) {

    if (form.value.name  === undefined || form.value.name === null) {
      return alert('nombre es requerido');
    }
    if (form.value.lastname === undefined || form.value.lastname === null) {
      return alert('apellido es requerido');
    }
    if (form.value.idCard === undefined || form.value.idCard === null) {
      return alert('cedula es requerido');
    }
    if (form.value.email === undefined || form.value.email === null) {
      return alert('email es requerido');
    }
    if (form.value.phone === undefined || form.value.phone === null) {
      return alert('telefono es requerido');
    }
    if (!validator.isEmail(form.value.email)) {
      return alert('debe ser email');
    }
    if (form.value._id) {
      this.userService.putUser(form.value).subscribe((res) => {
        this.getUsers();
        this.resetForm(form);
        this.onCloseModal(form);
      });
    } else {
      delete form.value._id;

      this.userService.postUser(form.value).subscribe((res: any) => {
        if (res.status === 'duplicate email') {
          return alert(
            `este email ya se encuentra registrado  ${form.value.email}`
          );
        }
        if (res.status === 'duplicate card') {
          return alert(
            `cedula ya se encuentra registrado  ${form.value.idCard}`
          );
        }
        this.getUsers();
        this.resetForm(form);
        this.onCloseModal(form);
      });
    }
  }
  resetForm(form?: NgForm | any) {
    if (form) {
      form.reset();
      this.userService.selectedUser = new User();
    }
  }
  getUsers() {
    this.onGetUser.emit('get user');
  }
}
